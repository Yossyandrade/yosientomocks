'use strict';

exports.getApiFeelingLastFeelingRegister = function(args, res, next) {
  /**
   * parameters expected in the args:
  * userId (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "tstatus" : 123456789,
  "content" : {
    "date" : 123456789,
    "Comment" : "aeiou",
    "image" : "aeiou",
    "latitud" : 1.3579000000000001069366817318950779736042022705078125,
    "longitud" : 1.3579000000000001069366817318950779736042022705078125,
    "idFeelingLevel1_2" : 123456789,
    "idFeelingLevel2_1" : 123456789,
    "idFeelingLevel1_1" : 123456789,
    "idFeelingLevel2_3" : 123456789,
    "Id" : "aeiou",
    "idFeelingLevel1_3" : 123456789,
    "idFeelingLevel2_2" : 123456789
  }
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.getApiPublicationSearch = function(args, res, next) {
  /**
   * parameters expected in the args:
  * word (String)
  * typeSearch (Long)
  **/
    var examples = {};
  examples['application/json'] = {
  "tstatus" : "aeiou",
  "content" : {
    "idUser" : "aeiou",
    "date" : 123456789,
    "nameUser" : "aeiou",
    "lastnameUser" : "aeiou",
    "linkImage" : "aeiou",
    "idPublication" : 123456789,
    "userAlias" : "aeiou",
    "DescriptionTypePublication" : "aeiou",
    "text" : "aeiou",
    "title" : "aeiou",
    "idTypePublication" : 123456789
  }
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.postApiFeelingRegister = function(args, res, next) {
  /**
   * parameters expected in the args:
  * userId (String)
  * feelingLevel1_1 (Long)
  * feelingLevel1_2 (Long)
  * feelingLevel1_3 (Long)
  * latitud (Double)
  * longitud (Double)
  * date (Long)
  * feelingLevel2_1 (Long)
  * feelingLevel2_2 (Long)
  * feelingLevel2_3 (Long)
  * comment (String)
  * image (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "tstatus" : 123456789,
  "content" : {
    "id" : "aeiou",
    "message" : "aeiou"
  }
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}


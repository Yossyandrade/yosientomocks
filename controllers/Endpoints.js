'use strict';

var url = require('url');


var Endpoints = require('./EndpointsService');


module.exports.getApiFeelingLastFeelingRegister = function getApiFeelingLastFeelingRegister (req, res, next) {
  Endpoints.getApiFeelingLastFeelingRegister(req.swagger.params, res, next);
};

module.exports.getApiPublicationSearch = function getApiPublicationSearch (req, res, next) {
  Endpoints.getApiPublicationSearch(req.swagger.params, res, next);
};

module.exports.postApiFeelingRegister = function postApiFeelingRegister (req, res, next) {
  Endpoints.postApiFeelingRegister(req.swagger.params, res, next);
};
